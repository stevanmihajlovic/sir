﻿namespace MasterThesis
{
    partial class AddHiddenLayer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxNeuronsPerLayer = new System.Windows.Forms.TextBox();
            this.checkBoxBiasNeuron = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxNeuronsPerLayer
            // 
            this.textBoxNeuronsPerLayer.Location = new System.Drawing.Point(239, 40);
            this.textBoxNeuronsPerLayer.Name = "textBoxNeuronsPerLayer";
            this.textBoxNeuronsPerLayer.Size = new System.Drawing.Size(45, 22);
            this.textBoxNeuronsPerLayer.TabIndex = 0;
            // 
            // checkBoxBiasNeuron
            // 
            this.checkBoxBiasNeuron.AutoSize = true;
            this.checkBoxBiasNeuron.Location = new System.Drawing.Point(126, 78);
            this.checkBoxBiasNeuron.Name = "checkBoxBiasNeuron";
            this.checkBoxBiasNeuron.Size = new System.Drawing.Size(146, 21);
            this.checkBoxBiasNeuron.TabIndex = 1;
            this.checkBoxBiasNeuron.Text = "Has a bias neuron";
            this.checkBoxBiasNeuron.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(99, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Neurons per layer:";
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(209, 133);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(75, 23);
            this.buttonAdd.TabIndex = 3;
            this.buttonAdd.Text = "Add";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(102, 133);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // AddHiddenLayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 197);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBoxBiasNeuron);
            this.Controls.Add(this.textBoxNeuronsPerLayer);
            this.Name = "AddHiddenLayer";
            this.Text = "AddHiddenLayer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxNeuronsPerLayer;
        private System.Windows.Forms.CheckBox checkBoxBiasNeuron;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonCancel;
    }
}