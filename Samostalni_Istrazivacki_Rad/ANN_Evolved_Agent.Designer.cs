﻿namespace MasterThesis
{
    partial class ANN_Evolved_Agent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.comboBoxParent = new System.Windows.Forms.ComboBox();
            this.comboBoxAdult = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxMutationRate = new System.Windows.Forms.TextBox();
            this.textBoxCrossOverRate = new System.Windows.Forms.TextBox();
            this.textBoxWeightBitLength = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPopulationSize = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.labelPoisonProbability = new System.Windows.Forms.Label();
            this.labelFoodProbability = new System.Windows.Forms.Label();
            this.textBoxPoisonProbability = new System.Windows.Forms.TextBox();
            this.textBoxFoodProbability = new System.Windows.Forms.TextBox();
            this.textBoxGenerationsNumber = new System.Windows.Forms.TextBox();
            this.checkBoxElitism = new System.Windows.Forms.CheckBox();
            this.textBoxTournamentSize = new System.Windows.Forms.TextBox();
            this.labelTournamentSize = new System.Windows.Forms.Label();
            this.textBoxOverProductionScale = new System.Windows.Forms.TextBox();
            this.labelOverProductionScale = new System.Windows.Forms.Label();
            this.labelMapDimensions = new System.Windows.Forms.Label();
            this.textBoxMapDimensions = new System.Windows.Forms.TextBox();
            this.comboBoxProblemType = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.labelMaxSteps = new System.Windows.Forms.Label();
            this.textBoxMaxSteps = new System.Windows.Forms.TextBox();
            this.labelAwardFactor = new System.Windows.Forms.Label();
            this.textBoxAwardFactor = new System.Windows.Forms.TextBox();
            this.labelPenaltyFactor = new System.Windows.Forms.Label();
            this.textBoxPenaltyFactor = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.buttonAddHiddenLayer = new System.Windows.Forms.Button();
            this.listBoxHiddenLayers = new System.Windows.Forms.ListBox();
            this.buttonClearHiddenLayers = new System.Windows.Forms.Button();
            this.labelStatus = new System.Windows.Forms.Label();
            this.buttonTrain = new System.Windows.Forms.Button();
            this.buttonTest = new System.Windows.Forms.Button();
            this.gridPanel = new System.Windows.Forms.Panel();
            this.labelSteps = new System.Windows.Forms.Label();
            this.comboBoxActivationFunction = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // chart1
            // 
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart1.Legends.Add(legend2);
            this.chart1.Location = new System.Drawing.Point(410, 438);
            this.chart1.Name = "chart1";
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Legend = "Legend1";
            series4.Name = "Max Fitness";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Legend = "Legend1";
            series5.Name = "Standard Deviation";
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.Legend = "Legend1";
            series6.Name = "Average Fitness";
            this.chart1.Series.Add(series4);
            this.chart1.Series.Add(series5);
            this.chart1.Series.Add(series6);
            this.chart1.Size = new System.Drawing.Size(763, 314);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chartPerformance";
            // 
            // comboBoxParent
            // 
            this.comboBoxParent.FormattingEnabled = true;
            this.comboBoxParent.Location = new System.Drawing.Point(173, 163);
            this.comboBoxParent.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxParent.Name = "comboBoxParent";
            this.comboBoxParent.Size = new System.Drawing.Size(188, 24);
            this.comboBoxParent.TabIndex = 28;
            this.comboBoxParent.SelectedIndexChanged += new System.EventHandler(this.comboBoxParent_SelectedIndexChanged);
            // 
            // comboBoxAdult
            // 
            this.comboBoxAdult.FormattingEnabled = true;
            this.comboBoxAdult.Location = new System.Drawing.Point(173, 225);
            this.comboBoxAdult.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxAdult.Name = "comboBoxAdult";
            this.comboBoxAdult.Size = new System.Drawing.Size(188, 24);
            this.comboBoxAdult.TabIndex = 27;
            this.comboBoxAdult.SelectedIndexChanged += new System.EventHandler(this.comboBoxAdult_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 167);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(159, 17);
            this.label7.TabIndex = 26;
            this.label7.Text = "Select Parent Selection:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 229);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 17);
            this.label6.TabIndex = 25;
            this.label6.Text = "Select Adult Selection:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 133);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(125, 17);
            this.label4.TabIndex = 24;
            this.label4.Text = "Set Mutation Rate:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 103);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 17);
            this.label3.TabIndex = 23;
            this.label3.Text = "Set Cross-Over Rate:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 73);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 17);
            this.label2.TabIndex = 22;
            this.label2.Text = "Set Weight Bit Length:";
            // 
            // textBoxMutationRate
            // 
            this.textBoxMutationRate.Location = new System.Drawing.Point(288, 133);
            this.textBoxMutationRate.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxMutationRate.Name = "textBoxMutationRate";
            this.textBoxMutationRate.Size = new System.Drawing.Size(73, 22);
            this.textBoxMutationRate.TabIndex = 21;
            this.textBoxMutationRate.Text = "0.01";
            // 
            // textBoxCrossOverRate
            // 
            this.textBoxCrossOverRate.Location = new System.Drawing.Point(288, 103);
            this.textBoxCrossOverRate.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxCrossOverRate.Name = "textBoxCrossOverRate";
            this.textBoxCrossOverRate.Size = new System.Drawing.Size(73, 22);
            this.textBoxCrossOverRate.TabIndex = 20;
            this.textBoxCrossOverRate.Text = "0.9";
            // 
            // textBoxWeightBitLength
            // 
            this.textBoxWeightBitLength.Location = new System.Drawing.Point(288, 73);
            this.textBoxWeightBitLength.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxWeightBitLength.Name = "textBoxWeightBitLength";
            this.textBoxWeightBitLength.Size = new System.Drawing.Size(73, 22);
            this.textBoxWeightBitLength.TabIndex = 19;
            this.textBoxWeightBitLength.Text = "10";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 43);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "Set Population Size:";
            // 
            // textBoxPopulationSize
            // 
            this.textBoxPopulationSize.Location = new System.Drawing.Point(288, 43);
            this.textBoxPopulationSize.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPopulationSize.Name = "textBoxPopulationSize";
            this.textBoxPopulationSize.Size = new System.Drawing.Size(73, 22);
            this.textBoxPopulationSize.TabIndex = 17;
            this.textBoxPopulationSize.Text = "100";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 13);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(169, 17);
            this.label5.TabIndex = 29;
            this.label5.Text = "Set Generations Number:";
            // 
            // labelPoisonProbability
            // 
            this.labelPoisonProbability.AutoSize = true;
            this.labelPoisonProbability.Location = new System.Drawing.Point(13, 539);
            this.labelPoisonProbability.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPoisonProbability.Name = "labelPoisonProbability";
            this.labelPoisonProbability.Size = new System.Drawing.Size(125, 17);
            this.labelPoisonProbability.TabIndex = 32;
            this.labelPoisonProbability.Text = "Poison Probability:";
            // 
            // labelFoodProbability
            // 
            this.labelFoodProbability.AutoSize = true;
            this.labelFoodProbability.Location = new System.Drawing.Point(13, 509);
            this.labelFoodProbability.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelFoodProbability.Name = "labelFoodProbability";
            this.labelFoodProbability.Size = new System.Drawing.Size(114, 17);
            this.labelFoodProbability.TabIndex = 33;
            this.labelFoodProbability.Text = "Food Probability:";
            // 
            // textBoxPoisonProbability
            // 
            this.textBoxPoisonProbability.Location = new System.Drawing.Point(288, 536);
            this.textBoxPoisonProbability.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPoisonProbability.Name = "textBoxPoisonProbability";
            this.textBoxPoisonProbability.Size = new System.Drawing.Size(73, 22);
            this.textBoxPoisonProbability.TabIndex = 30;
            this.textBoxPoisonProbability.Text = "0.33";
            // 
            // textBoxFoodProbability
            // 
            this.textBoxFoodProbability.Location = new System.Drawing.Point(288, 506);
            this.textBoxFoodProbability.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxFoodProbability.Name = "textBoxFoodProbability";
            this.textBoxFoodProbability.Size = new System.Drawing.Size(73, 22);
            this.textBoxFoodProbability.TabIndex = 31;
            this.textBoxFoodProbability.Text = "0.33";
            // 
            // textBoxGenerationsNumber
            // 
            this.textBoxGenerationsNumber.Location = new System.Drawing.Point(288, 13);
            this.textBoxGenerationsNumber.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxGenerationsNumber.Name = "textBoxGenerationsNumber";
            this.textBoxGenerationsNumber.Size = new System.Drawing.Size(73, 22);
            this.textBoxGenerationsNumber.TabIndex = 34;
            this.textBoxGenerationsNumber.Text = "100";
            // 
            // checkBoxElitism
            // 
            this.checkBoxElitism.AutoSize = true;
            this.checkBoxElitism.Checked = true;
            this.checkBoxElitism.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxElitism.Location = new System.Drawing.Point(13, 692);
            this.checkBoxElitism.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxElitism.Name = "checkBoxElitism";
            this.checkBoxElitism.Size = new System.Drawing.Size(70, 21);
            this.checkBoxElitism.TabIndex = 35;
            this.checkBoxElitism.Text = "Elitism";
            this.checkBoxElitism.UseVisualStyleBackColor = true;
            // 
            // textBoxTournamentSize
            // 
            this.textBoxTournamentSize.Location = new System.Drawing.Point(288, 195);
            this.textBoxTournamentSize.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxTournamentSize.Name = "textBoxTournamentSize";
            this.textBoxTournamentSize.Size = new System.Drawing.Size(73, 22);
            this.textBoxTournamentSize.TabIndex = 37;
            this.textBoxTournamentSize.Text = "5";
            this.textBoxTournamentSize.Visible = false;
            // 
            // labelTournamentSize
            // 
            this.labelTournamentSize.AutoSize = true;
            this.labelTournamentSize.Location = new System.Drawing.Point(13, 198);
            this.labelTournamentSize.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTournamentSize.Name = "labelTournamentSize";
            this.labelTournamentSize.Size = new System.Drawing.Size(120, 17);
            this.labelTournamentSize.TabIndex = 36;
            this.labelTournamentSize.Text = "Tournament Size:";
            this.labelTournamentSize.Visible = false;
            // 
            // textBoxOverProductionScale
            // 
            this.textBoxOverProductionScale.Location = new System.Drawing.Point(288, 257);
            this.textBoxOverProductionScale.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxOverProductionScale.Name = "textBoxOverProductionScale";
            this.textBoxOverProductionScale.Size = new System.Drawing.Size(73, 22);
            this.textBoxOverProductionScale.TabIndex = 39;
            this.textBoxOverProductionScale.Text = "2";
            this.textBoxOverProductionScale.Visible = false;
            // 
            // labelOverProductionScale
            // 
            this.labelOverProductionScale.AutoSize = true;
            this.labelOverProductionScale.Location = new System.Drawing.Point(13, 260);
            this.labelOverProductionScale.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelOverProductionScale.Name = "labelOverProductionScale";
            this.labelOverProductionScale.Size = new System.Drawing.Size(154, 17);
            this.labelOverProductionScale.TabIndex = 38;
            this.labelOverProductionScale.Text = "Over Production Scale:";
            this.labelOverProductionScale.Visible = false;
            // 
            // labelMapDimensions
            // 
            this.labelMapDimensions.AutoSize = true;
            this.labelMapDimensions.Location = new System.Drawing.Point(13, 569);
            this.labelMapDimensions.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelMapDimensions.Name = "labelMapDimensions";
            this.labelMapDimensions.Size = new System.Drawing.Size(116, 17);
            this.labelMapDimensions.TabIndex = 41;
            this.labelMapDimensions.Text = "Map Dimensions:";
            // 
            // textBoxMapDimensions
            // 
            this.textBoxMapDimensions.Location = new System.Drawing.Point(288, 566);
            this.textBoxMapDimensions.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxMapDimensions.Name = "textBoxMapDimensions";
            this.textBoxMapDimensions.Size = new System.Drawing.Size(73, 22);
            this.textBoxMapDimensions.TabIndex = 40;
            this.textBoxMapDimensions.Text = "10";
            // 
            // comboBoxProblemType
            // 
            this.comboBoxProblemType.FormattingEnabled = true;
            this.comboBoxProblemType.Location = new System.Drawing.Point(173, 287);
            this.comboBoxProblemType.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxProblemType.Name = "comboBoxProblemType";
            this.comboBoxProblemType.Size = new System.Drawing.Size(188, 24);
            this.comboBoxProblemType.TabIndex = 43;
            this.comboBoxProblemType.SelectedIndexChanged += new System.EventHandler(this.comboBoxProblemType_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 291);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 17);
            this.label11.TabIndex = 42;
            this.label11.Text = "Problem Type:";
            // 
            // labelMaxSteps
            // 
            this.labelMaxSteps.AutoSize = true;
            this.labelMaxSteps.Location = new System.Drawing.Point(13, 599);
            this.labelMaxSteps.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelMaxSteps.Name = "labelMaxSteps";
            this.labelMaxSteps.Size = new System.Drawing.Size(77, 17);
            this.labelMaxSteps.TabIndex = 45;
            this.labelMaxSteps.Text = "Max Steps:";
            // 
            // textBoxMaxSteps
            // 
            this.textBoxMaxSteps.Location = new System.Drawing.Point(288, 596);
            this.textBoxMaxSteps.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxMaxSteps.Name = "textBoxMaxSteps";
            this.textBoxMaxSteps.Size = new System.Drawing.Size(73, 22);
            this.textBoxMaxSteps.TabIndex = 44;
            this.textBoxMaxSteps.Text = "60";
            // 
            // labelAwardFactor
            // 
            this.labelAwardFactor.AutoSize = true;
            this.labelAwardFactor.Location = new System.Drawing.Point(13, 629);
            this.labelAwardFactor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelAwardFactor.Name = "labelAwardFactor";
            this.labelAwardFactor.Size = new System.Drawing.Size(91, 17);
            this.labelAwardFactor.TabIndex = 47;
            this.labelAwardFactor.Text = "Award factor:";
            // 
            // textBoxAwardFactor
            // 
            this.textBoxAwardFactor.Location = new System.Drawing.Point(288, 626);
            this.textBoxAwardFactor.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxAwardFactor.Name = "textBoxAwardFactor";
            this.textBoxAwardFactor.Size = new System.Drawing.Size(73, 22);
            this.textBoxAwardFactor.TabIndex = 46;
            this.textBoxAwardFactor.Text = "5";
            // 
            // labelPenaltyFactor
            // 
            this.labelPenaltyFactor.AutoSize = true;
            this.labelPenaltyFactor.Location = new System.Drawing.Point(13, 659);
            this.labelPenaltyFactor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPenaltyFactor.Name = "labelPenaltyFactor";
            this.labelPenaltyFactor.Size = new System.Drawing.Size(99, 17);
            this.labelPenaltyFactor.TabIndex = 49;
            this.labelPenaltyFactor.Text = "Penalty factor:";
            // 
            // textBoxPenaltyFactor
            // 
            this.textBoxPenaltyFactor.Location = new System.Drawing.Point(288, 656);
            this.textBoxPenaltyFactor.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPenaltyFactor.Name = "textBoxPenaltyFactor";
            this.textBoxPenaltyFactor.Size = new System.Drawing.Size(73, 22);
            this.textBoxPenaltyFactor.TabIndex = 48;
            this.textBoxPenaltyFactor.Text = "30";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 353);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(137, 17);
            this.label8.TabIndex = 50;
            this.label8.Text = "ANN Hidden Layers:";
            // 
            // buttonAddHiddenLayer
            // 
            this.buttonAddHiddenLayer.Location = new System.Drawing.Point(288, 350);
            this.buttonAddHiddenLayer.Name = "buttonAddHiddenLayer";
            this.buttonAddHiddenLayer.Size = new System.Drawing.Size(73, 23);
            this.buttonAddHiddenLayer.TabIndex = 51;
            this.buttonAddHiddenLayer.Text = "Add";
            this.buttonAddHiddenLayer.UseVisualStyleBackColor = true;
            this.buttonAddHiddenLayer.Click += new System.EventHandler(this.buttonAddHiddenLayer_Click);
            // 
            // listBoxHiddenLayers
            // 
            this.listBoxHiddenLayers.FormattingEnabled = true;
            this.listBoxHiddenLayers.ItemHeight = 16;
            this.listBoxHiddenLayers.Location = new System.Drawing.Point(12, 379);
            this.listBoxHiddenLayers.Name = "listBoxHiddenLayers";
            this.listBoxHiddenLayers.Size = new System.Drawing.Size(349, 116);
            this.listBoxHiddenLayers.TabIndex = 52;
            // 
            // buttonClearHiddenLayers
            // 
            this.buttonClearHiddenLayers.Location = new System.Drawing.Point(173, 350);
            this.buttonClearHiddenLayers.Name = "buttonClearHiddenLayers";
            this.buttonClearHiddenLayers.Size = new System.Drawing.Size(73, 23);
            this.buttonClearHiddenLayers.TabIndex = 53;
            this.buttonClearHiddenLayers.Text = "Clear";
            this.buttonClearHiddenLayers.UseVisualStyleBackColor = true;
            this.buttonClearHiddenLayers.Click += new System.EventHandler(this.buttonClearHiddenLayers_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(128, 693);
            this.labelStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(118, 17);
            this.labelStatus.TabIndex = 54;
            this.labelStatus.Text = "Status: Untrained";
            // 
            // buttonTrain
            // 
            this.buttonTrain.Location = new System.Drawing.Point(173, 724);
            this.buttonTrain.Name = "buttonTrain";
            this.buttonTrain.Size = new System.Drawing.Size(75, 23);
            this.buttonTrain.TabIndex = 55;
            this.buttonTrain.Text = "Train";
            this.buttonTrain.UseVisualStyleBackColor = true;
            this.buttonTrain.Click += new System.EventHandler(this.buttonTrain_Click);
            // 
            // buttonTest
            // 
            this.buttonTest.Enabled = false;
            this.buttonTest.Location = new System.Drawing.Point(286, 724);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(75, 23);
            this.buttonTest.TabIndex = 56;
            this.buttonTest.Text = "Test";
            this.buttonTest.UseVisualStyleBackColor = true;
            this.buttonTest.Click += new System.EventHandler(this.buttonTest_Click);
            // 
            // gridPanel
            // 
            this.gridPanel.Location = new System.Drawing.Point(591, 43);
            this.gridPanel.Margin = new System.Windows.Forms.Padding(4);
            this.gridPanel.Name = "gridPanel";
            this.gridPanel.Size = new System.Drawing.Size(400, 369);
            this.gridPanel.TabIndex = 57;
            this.gridPanel.Visible = false;
            // 
            // labelSteps
            // 
            this.labelSteps.AutoSize = true;
            this.labelSteps.Location = new System.Drawing.Point(656, 12);
            this.labelSteps.Name = "labelSteps";
            this.labelSteps.Size = new System.Drawing.Size(0, 17);
            this.labelSteps.TabIndex = 58;
            // 
            // comboBoxActivationFunction
            // 
            this.comboBoxActivationFunction.FormattingEnabled = true;
            this.comboBoxActivationFunction.Location = new System.Drawing.Point(173, 319);
            this.comboBoxActivationFunction.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxActivationFunction.Name = "comboBoxActivationFunction";
            this.comboBoxActivationFunction.Size = new System.Drawing.Size(188, 24);
            this.comboBoxActivationFunction.TabIndex = 60;
            this.comboBoxActivationFunction.SelectedIndexChanged += new System.EventHandler(this.comboBoxActivationFunction_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 323);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(131, 17);
            this.label9.TabIndex = 59;
            this.label9.Text = "Activation Function:";
            // 
            // ANN_Evolved_Agent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1185, 764);
            this.Controls.Add(this.comboBoxActivationFunction);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.labelSteps);
            this.Controls.Add(this.gridPanel);
            this.Controls.Add(this.buttonTest);
            this.Controls.Add(this.buttonTrain);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.buttonClearHiddenLayers);
            this.Controls.Add(this.listBoxHiddenLayers);
            this.Controls.Add(this.buttonAddHiddenLayer);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.labelPenaltyFactor);
            this.Controls.Add(this.textBoxPenaltyFactor);
            this.Controls.Add(this.labelAwardFactor);
            this.Controls.Add(this.textBoxAwardFactor);
            this.Controls.Add(this.labelMaxSteps);
            this.Controls.Add(this.textBoxMaxSteps);
            this.Controls.Add(this.comboBoxProblemType);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.labelMapDimensions);
            this.Controls.Add(this.textBoxMapDimensions);
            this.Controls.Add(this.textBoxOverProductionScale);
            this.Controls.Add(this.labelOverProductionScale);
            this.Controls.Add(this.textBoxTournamentSize);
            this.Controls.Add(this.labelTournamentSize);
            this.Controls.Add(this.checkBoxElitism);
            this.Controls.Add(this.textBoxGenerationsNumber);
            this.Controls.Add(this.labelPoisonProbability);
            this.Controls.Add(this.labelFoodProbability);
            this.Controls.Add(this.textBoxPoisonProbability);
            this.Controls.Add(this.textBoxFoodProbability);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBoxParent);
            this.Controls.Add(this.comboBoxAdult);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxMutationRate);
            this.Controls.Add(this.textBoxCrossOverRate);
            this.Controls.Add(this.textBoxWeightBitLength);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxPopulationSize);
            this.Controls.Add(this.chart1);
            this.Name = "ANN_Evolved_Agent";
            this.Text = "ANN Evolved Agent";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.ComboBox comboBoxParent;
        private System.Windows.Forms.ComboBox comboBoxAdult;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxMutationRate;
        private System.Windows.Forms.TextBox textBoxCrossOverRate;
        public System.Windows.Forms.TextBox textBoxWeightBitLength;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPopulationSize;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelPoisonProbability;
        private System.Windows.Forms.Label labelFoodProbability;
        private System.Windows.Forms.TextBox textBoxPoisonProbability;
        private System.Windows.Forms.TextBox textBoxFoodProbability;
        private System.Windows.Forms.TextBox textBoxGenerationsNumber;
        private System.Windows.Forms.CheckBox checkBoxElitism;
        private System.Windows.Forms.TextBox textBoxTournamentSize;
        private System.Windows.Forms.Label labelTournamentSize;
        private System.Windows.Forms.TextBox textBoxOverProductionScale;
        private System.Windows.Forms.Label labelOverProductionScale;
        private System.Windows.Forms.Label labelMapDimensions;
        private System.Windows.Forms.TextBox textBoxMapDimensions;
        private System.Windows.Forms.ComboBox comboBoxProblemType;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelMaxSteps;
        private System.Windows.Forms.TextBox textBoxMaxSteps;
        private System.Windows.Forms.Label labelAwardFactor;
        private System.Windows.Forms.TextBox textBoxAwardFactor;
        private System.Windows.Forms.Label labelPenaltyFactor;
        private System.Windows.Forms.TextBox textBoxPenaltyFactor;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonAddHiddenLayer;
        public System.Windows.Forms.ListBox listBoxHiddenLayers;
        private System.Windows.Forms.Button buttonClearHiddenLayers;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Button buttonTrain;
        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.Panel gridPanel;
        private System.Windows.Forms.Label labelSteps;
        private System.Windows.Forms.ComboBox comboBoxActivationFunction;
        private System.Windows.Forms.Label label9;
    }
}

