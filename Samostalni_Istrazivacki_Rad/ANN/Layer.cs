﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.ANN
{
    public class Layer
    {
        /// <summary>
        /// Gets or sets a value representing Neurons that Layer consists of.
        /// </summary>
        public List<Neuron> Neurons { get; set; }

        /// <summary>
        /// Initializes Input layer consisting of specified number of Input Neurons and specified activation function.
        /// If not specified then it initializes with Linear activation function.
        /// </summary>
        /// <param name="numberOfNeuronsInLayer">Specified number of Input Neurons.</param>
        public Layer(int numberOfNeuronsInLayer)
        {
            Neurons = new List<Neuron>();
            for (int i = 0; i < numberOfNeuronsInLayer; i++)
            {
                Neurons.Add(new Neuron());
            }
        }

        /// <summary>
        /// Initializes Hidden or Output Layer cosisting of specified number of Neurons and Bias Neurons.
        /// If not specified then it initializes with Linear activation function.
        /// Each Neuron has the same specified list of predecessors but different weights to them.
        /// </summary>
        /// <param name="numberOfNeuronsInLayer">Specified number of Neurons.</param>
        /// <param name="numberOfBiasNeurons">Specified number of Bias Neurons.</param>
        /// <param name="predecessors">Specified list of predecessors.</param>
        /// <param name="weightsBetweenLayers">Specified list of weights.</param>
        public Layer(int numberOfNeuronsInLayer, int numberOfBiasNeurons, List<Neuron> predecessors, List<Synapse> weightsBetweenLayers)
        {
            Neurons = new List<Neuron>();
            int weightsPerNeuron = weightsBetweenLayers.Count / numberOfNeuronsInLayer;
            int i;
            for (i = 0; i < numberOfNeuronsInLayer; i++)
            {
                Neurons.Add(new Neuron(predecessors));
                Neurons[i].UpdateWeights(weightsBetweenLayers.GetRange(i * weightsPerNeuron, weightsPerNeuron));
            }
            for (int j = 0; j < numberOfBiasNeurons; j++, i++)
            {
                Neurons.Add(new Neuron(1));
            }
        }
    }
}
