﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.ANN
{
    public class Grid
    {
        /// <summary>
        /// Gets or sets a value representing probability that Tile in the Map will contain Food.
        /// </summary>
        public double FoodProbability { get; private set; }

        /// <summary>
        /// Gets or sets a value representing probability that Tile in the Map will contain Poison.
        /// </summary>
        public double PoisonProbability { get; private set; }

        /// <summary>
        /// Gets or sets a value representing dimension of the Map.
        /// </summary>
        public int Dimension { get; private set; }

        /// <summary>
        /// Gets or sets a value representing Tiles that Map consists of.
        /// </summary>
        public TileContent[][] Tiles { get; private set; }

        /// <summary>
        /// Used for instantiating copies.
        /// </summary>
        public Grid()
        {

        }

        /// <summary>
        /// Instantiates Map with the specified food and poison probability and dimensions. If not specified, it uses default values.
        /// </summary>
        /// <param name="probF">Specified food probability.</param>
        /// <param name="probP">Specified poison probability.</param>
        /// <param name="N">Specified dimension of the Map.</param>
        public Grid(double probF = 0.33, double probP = 0.33, int N = 10)
        {
            FoodProbability = probF;
            PoisonProbability = probP;
            if (probF + probP > 1)
            {
                FoodProbability = PoisonProbability = 0.33;
            }
            Dimension = N;

            Tiles = new TileContent[Dimension][];
            for (int i = 0; i < Dimension; i++)
            {
                Tiles[i] = new TileContent[Dimension];

                for (int j = 0; j < Dimension; j++)
                {
                    double temp = Program.RANDOM.NextDouble();
                    temp = temp - FoodProbability;
                    if (temp < 0)
                    {
                        Tiles[i][j] = TileContent.Food;
                        continue;
                    }
                    temp = temp - PoisonProbability;
                    if (temp < 0)
                    {
                        Tiles[i][j] = TileContent.Poison;
                    }
                    else
                    {
                        Tiles[i][j] = TileContent.Nothing;
                    }
                }
            }
        }

        /// <summary>
        /// Creates an identical copy of the Map. Used for reversing changes made by Calculating Fitness of the Agent.
        /// </summary>
        /// <returns></returns>
        public Grid MakeACopy()
        {
            Grid temp = new Grid();
            temp.FoodProbability = this.FoodProbability;
            temp.PoisonProbability = this.PoisonProbability;
            temp.Dimension = this.Dimension;
            temp.Tiles = new TileContent[Dimension][];
            for (int i = 0; i < Dimension; i++)
            {
                temp.Tiles[i] = new TileContent[Dimension];

                for (int j = 0; j < Dimension; j++)
                {
                    temp.Tiles[i][j] = this.Tiles[i][j];
                }
            }
            return temp;
        }

        /// <summary>
        /// Gets the tile in the Map above the provided coordinates.
        /// </summary>
        /// <param name="xCoordinate">provided X coordinate.</param>
        /// <param name="yCoordinate">provided Y coordinate.</param>
        /// <returns></returns>
        public TileContent GetTopTile(int xCoordinate, int yCoordinate)
        {
            return Tiles[xCoordinate][(yCoordinate - 1 + Dimension) % Dimension];
        }

        /// <summary>
        /// Gets the tile in the Map below the provided coordinates.
        /// </summary>
        /// <param name="xCoordinate">provided X coordinate.</param>
        /// <param name="yCoordinate">provided Y coordinate.</param>
        /// <returns></returns>
        public TileContent GetBottomTile(int xCoordinate, int yCoordinate)
        {
            return Tiles[xCoordinate][(yCoordinate + 1) % Dimension];
        }

        /// <summary>
        /// Gets the tile in the Map right from the provided coordinates.
        /// </summary>
        /// <param name="xCoordinate">provided X coordinate.</param>
        /// <param name="yCoordinate">provided Y coordinate.</param>
        /// <returns></returns>
        public TileContent GetRightTile(int xCoordinate, int yCoordinate)
        {
            return Tiles[(xCoordinate + 1) % Dimension][yCoordinate];
        }

        /// <summary>
        /// Gets the tile in the Map left from the provided coordinates.
        /// </summary>
        /// <param name="xCoordinate">provided X coordinate.</param>
        /// <param name="yCoordinate">provided Y coordinate.</param>
        /// <returns></returns>
        public TileContent GetLeftTile(int xCoordinate, int yCoordinate)
        {
            return Tiles[(xCoordinate - 1 + Dimension) % Dimension][yCoordinate];
        }
    }
}
