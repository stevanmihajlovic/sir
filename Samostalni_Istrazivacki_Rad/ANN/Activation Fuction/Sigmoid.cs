﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.ANN.Activation_Fuction
{
    public class Sigmoid : IActivationFunction
    {
        public double ReCalculateValue(double neuronValue)
        {
            return (Math.Exp(neuronValue) - Math.Exp(-neuronValue)) / (Math.Exp(neuronValue) + Math.Exp(-neuronValue));
        }
    }
}
