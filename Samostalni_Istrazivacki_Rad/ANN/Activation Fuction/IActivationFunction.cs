﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.ANN.Activation_Fuction
{
    public interface IActivationFunction
    {
        /// <summary>
        /// Recalculates the specified Neuron value that will become the final value of the Neuron.
        /// </summary>
        /// <param name="neuronValue">specified Neuron value.</param>
        /// <returns></returns>
        double ReCalculateValue(double neuronValue);
    }
}
