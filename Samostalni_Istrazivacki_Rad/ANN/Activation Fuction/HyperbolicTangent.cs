﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.ANN.Activation_Fuction
{
    public class HyperbolicTangent : IActivationFunction
    {
        public double ReCalculateValue(double neuronValue)
        {
            return (1.0 / (1.0 + Math.Exp(-neuronValue)));
        }
    }
}
