﻿using MasterThesis.ANN.Activation_Fuction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.ANN
{
    public class Neuron
    {
        private List<Synapse> _weights;
        private List<Neuron> _predecessors;
        private double _value;
        private bool _isValueCalculated;
        private IActivationFunction _activationFunction;

        /// <summary>
        /// Initializes Input Neuron with specified activation function.
        /// If not specified then it initializes with Linear activation function.
        /// </summary>
        public Neuron()
        {
            _weights = null;
            _predecessors = null;
            _isValueCalculated = false;
            SetActivationFunction();
        }

        /// <summary>
        /// Initializes Neuron with specified value and specified activation function.
        /// If not specified then it initializes with Linear activation function.
        /// This ctor should be used for Bias neurons and value should be 1.
        /// </summary>
        /// <param name="value">Specified value.</param>
        public Neuron(double value)
            : this()
        {
            UpdateValue(value);
        }

        /// <summary>
        /// Initializes Hidden or Output neuron with list of predecessors and specified Activation function.
        /// If not specified then it initializes with Linear activation function.
        /// </summary>
        /// <param name="predecessors">List of predecessors.</param>
        public Neuron(List<Neuron> predecessors)
        {
            _predecessors = predecessors;
            _value = 0;
            _isValueCalculated = false;
            SetActivationFunction();
        }

        /// <summary>
        /// Instantiates _activationFunction based on specified activation function.
        /// </summary>
        private void SetActivationFunction()
        {
            switch (Program.CHOSEN_ACTIVATION_FUNCTION)
            {
                case ActivationFunction.Linear:
                    {
                        _activationFunction = new Linear();
                        break;
                    }
                case ActivationFunction.Sigmoid:
                    {
                        _activationFunction = new Sigmoid();
                        break;
                    }
                case ActivationFunction.HyperbolicTangent:
                    {
                        _activationFunction = new HyperbolicTangent();
                        break;
                    }
                default:
                    throw new NotImplementedException("Activation function not implemented.");
            }
        }

        /// <summary>
        /// Recursevelly calculates value of this Neuron based on values of all predecessors and their predecessors.
        /// </summary>
        /// <returns></returns>
        public double CalculateValue()
        {
            if (_isValueCalculated || _predecessors == null)
                return _value;
            else
            {
                for (int i = 0; i < _predecessors.Count; i++)
                {
                    _value += _predecessors[i].CalculateValue() * _weights[i].Weight;
                }
                _value = _activationFunction.ReCalculateValue(_value);
                _isValueCalculated = true;
                return _value;
            }
        }

        // Old CalculateValue method that did not have _isValueCalculated and was really slow

        //public double CalculateValue()
        //{
        //    if (_predecessors == null)
        //        return _value;
        //    else
        //    {
        //        _value = 0;
        //        for (int i = 0; i < _predecessors.Count; i++)
        //        {
        //            _value += _predecessors[i].CalculateValue() * _weights[i].Weight;
        //        }
        //        _value = _activationFunction.ReCalculateValue(_value);
        //        _isValueCalculated = true;
        //        return _value;
        //    }
        //}

        /// <summary>
        /// Resets value of Neuron.
        /// </summary>
        public void ResetValue()
        {
            _value = 0;
            _isValueCalculated = false;
        }

        /// <summary>
        /// Sets value of Neuron to the specified value.
        /// </summary>
        /// <param name="value">Specified value.</param>
        public void UpdateValue(double value)
        {
            _value = value;
            _isValueCalculated = true;
        }

        /// <summary>
        /// Sets the specified incoming weights to be used by this Neuron in combination with its predecessors.
        /// </summary>
        /// <param name="weights">Specified incoming weights.</param>
        public void UpdateWeights(List<Synapse> weights)
        {
            if (_predecessors.Count != weights.Count)
                throw new Exception("Total number of predecessor neurons does not match count of passed list of weights");
            _weights = weights;
        }
    }
}
