﻿using MasterThesis.ANN.Neural_Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.ANN
{
    public class FeedForwardANN : ArtificialNeuralNetwork
    {
        private List<int> _neuronsPerHiddenLayer;
        private List<int> _biasesPerHiddenLayer;

        /// <summary>
        /// Instantiates Feed Forward ANN in which all neurons are fully connected.
        /// </summary>
        /// <param name="numberOfInputNeurons">Number of neurons in input layer.</param>
        /// <param name="numberOfOutputNeurons">Number of neurons in output layer.</param>
        /// <param name="neuronsPerHiddenLayer">Number of neurons for each hidden layer.</param>
        /// <param name="biasesPerHiddenLayer">Number of Bias neurons for each hidden layer.</param>
        public FeedForwardANN(int numberOfInputNeurons, int numberOfOutputNeurons, 
                              List<int> neuronsPerHiddenLayer, List<int> biasesPerHiddenLayer)
        {
            _neuronsPerHiddenLayer = neuronsPerHiddenLayer;
            _biasesPerHiddenLayer = biasesPerHiddenLayer;
            AllWeights = new List<Synapse>();
            TotalNumberOfWeights = 0;

            InputLayer = new Layer(numberOfInputNeurons);

            if (_neuronsPerHiddenLayer.Count != 0)
            {
                HiddenLayers = new List<Layer>(_neuronsPerHiddenLayer.Count);
                HiddenLayers.Add(InstantiateNewLayer(_neuronsPerHiddenLayer[0], _biasesPerHiddenLayer[0], numberOfInputNeurons, 0, InputLayer));
        
                for (int i = 1; i < _neuronsPerHiddenLayer.Count; i++)
                {
                    HiddenLayers.Add(InstantiateNewLayer(_neuronsPerHiddenLayer[i], _biasesPerHiddenLayer[i], _neuronsPerHiddenLayer[i - 1], _biasesPerHiddenLayer[i - 1], HiddenLayers[i - 1]));
                }
                OutputLayer = InstantiateNewLayer(numberOfOutputNeurons, 0, _neuronsPerHiddenLayer[_neuronsPerHiddenLayer.Count - 1], _biasesPerHiddenLayer[_neuronsPerHiddenLayer.Count - 1], HiddenLayers.Last());
            }
            else
            {
                OutputLayer = InstantiateNewLayer(numberOfOutputNeurons, 0, numberOfInputNeurons, 0, InputLayer);
            }
        }

        /// <summary>
        /// Instantiates Hidden or Output Layer that consists of specified number of Neurons and Bias Neurons
        /// </summary>
        /// <param name="numberOfNeuronsInCurrentLayer">Specified number of Neurons in current Layer</param>
        /// <param name="numberOfBiasNeuronsInCurrentLayer">Specified number of Bias Neurons in current Layer</param>
        /// <param name="numberOfNeuronsInPreviousLayer">Specified number of Neurons in previous Layer</param>
        /// <param name="numberOfBiasNeuronsInPreviousLayer">Specified number of Bias Neurons in previous Layer</param>
        /// <param name="predecessors"></param>
        /// <returns></returns>
        public Layer InstantiateNewLayer(int numberOfNeuronsInCurrentLayer, int numberOfBiasNeuronsInCurrentLayer, int numberOfNeuronsInPreviousLayer, int numberOfBiasNeuronsInPreviousLayer, Layer predecessors)
        {
            int numberOfWeightsBetweenTwoLayers = numberOfNeuronsInCurrentLayer * (numberOfNeuronsInPreviousLayer + numberOfBiasNeuronsInPreviousLayer);
            for (int j = 0; j < numberOfWeightsBetweenTwoLayers; j++)
            {
                AllWeights.Add(new Synapse());
            }
            Layer toReturn = new Layer(numberOfNeuronsInCurrentLayer, numberOfBiasNeuronsInCurrentLayer, predecessors.Neurons, AllWeights.GetRange(TotalNumberOfWeights, numberOfWeightsBetweenTwoLayers));
            TotalNumberOfWeights += numberOfWeightsBetweenTwoLayers;
            return toReturn;
        }
    }
}
