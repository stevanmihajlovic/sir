﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.ANN
{
    public class Synapse
    {
        /// <summary>
        /// Gets or sets a value representing weight of the Synapse.
        /// </summary>
        public double Weight { get; set; }
    }
}
