﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterThesis.ANN;
using MasterThesis.Genetic_Algorithm.Adult_Selection;
using MasterThesis.Genetic_Algorithm.Individuals;
using MasterThesis.Genetic_Algorithm.Parent_Selection;

namespace MasterThesis.Genetic_Algorithm
{
    public class Population
    {
        /// <summary>
        /// Gets or sets a value representing all Individuals that Population consists of.
        /// </summary>
        public List<Individual> Individuals { get; private set; }

        /// <summary>
        /// Gets or sets a value representing Population size.
        /// </summary>
        public int PopulationSize { get; private set; }

        /// <summary>
        /// Gets or sets a value representing Data property Total Fitness of the population.
        /// </summary>
        public double TotalFitness { get; private set; }

        /// <summary>
        /// Gets or sets a value representing Data property Average Fitness of the population.
        /// </summary>
        public double AverageFitness { get; private set; }

        /// <summary>
        /// Gets or sets a value representing Data property Standard Deviation of Fitness of the population.
        /// </summary>
        public double StandardDeviation { get; private set; }

        /// <summary>
        /// Gets or sets a value representing Data property Individual with highest Fitness in the population.
        /// </summary>
        public Individual MaxFitIndividual { get; private set; }

        /// <summary>
        /// Gets or sets a value representing Data property Individual with lowest Fitness in the population.
        /// </summary>
        public Individual MinFitIndividual { get; private set; }

        private IAdultSelection _adultSelectionInterface;
        private IParentSelection _parentSelectionInterface;

        /// <summary>
        /// Creates an Population that consists of specified population size Individuals.
        /// </summary>
        /// <param name="size">Specified population size</param>
        /// <param name="weightBitLength"></param>
        public Population(int size, int weightBitLength)
        {
            PopulationSize = size;
            Individuals = new List<Individual>(PopulationSize);

            switch (Program.CHOSEN_PROBLEM_TYPE)
            {
                case ProblemType.ANNBitAgent:
                    {
                        for (int i = 0; i < PopulationSize; i++)
                            Individuals.Add(new AgentBitPhenotype(Program.ANN, weightBitLength));
                        break;
                    }
                case ProblemType.ANNDoubleAgent:
                    {
                        for (int i = 0; i < PopulationSize; i++)
                            Individuals.Add(new AgentDoublePhenotype(Program.ANN));
                        break;
                    }
                default:
                    throw new NotImplementedException("Problem type not implemented.");
            }

            switch (Program.CHOSEN_ADULT_SELECTION)
            {
                case AdultSelection.FullGenerationReplacement:
                    {
                        _adultSelectionInterface = new FullGenerationReplacement();
                        break;
                    }
                case AdultSelection.OverProduction:
                    {
                        _adultSelectionInterface = new OverProduction();
                        break;
                    }
                case AdultSelection.GenerationMixing:
                    {
                        _adultSelectionInterface = new GenerationMixing();
                        break;
                    }
                default:
                    throw new NotImplementedException("Adult selection not implemented.");
            }

            switch (Program.CHOSEN_PARENT_SELECTION)
            {
                case ParentSelection.Tournament:
                    {
                        _parentSelectionInterface = new Tournament();
                        break;
                    }
                //case ParentSelection.FitnessProportionate:
                //    {
                //        _parentSelectionInterface = new FitnessProportionate();
                //        break;
                //    }
                case ParentSelection.SigmaScaling:
                    {
                        _parentSelectionInterface = new SigmaScaling();
                        break;
                    }
                //case ParentSelection.Rank:
                //    {
                //        _parentSelectionInterface = new Rank();
                //        break;
                //    }
                default:
                    throw new NotImplementedException("Parent selection not implemented.");
            }
        }

        /// <summary>
        /// Creates an empty Population of specified population size.
        /// Should be used for creating Offspring population in the Adult Selecionn mechanisms.
        /// </summary>
        /// <param name="populationSize">Specified population size</param>
        public Population(int populationSize)
        {
            PopulationSize = populationSize;
            Individuals = new List<Individual>(PopulationSize);
        }

        /// <summary>
        /// Updates fitness for all Individuals and updates all Data properties in the population.
        /// </summary>
        public void UpdateFitnessAndPopulationData()
        {
            foreach (Individual individual in Individuals)
                individual.CalculateFitness();
            UpdatePopulationData();
        }

        /// <summary>
        /// Updates all Data properties in the population.
        /// </summary>
        public void UpdatePopulationData()
        {
            TotalFitness = 0;
            MaxFitIndividual = MinFitIndividual = Individuals.First();
            foreach (Individual individual in Individuals)
            {
                TotalFitness += individual.Fitness;
                if (MaxFitIndividual.Fitness < individual.Fitness)
                    MaxFitIndividual = individual;
                if (MinFitIndividual.Fitness > individual.Fitness)
                    MinFitIndividual = individual;
            }
            AverageFitness = TotalFitness / PopulationSize;
            StandardDeviation = 0;
            foreach (Individual individual in Individuals)
                StandardDeviation += Math.Pow(individual.Fitness - AverageFitness, 2);
            StandardDeviation /= PopulationSize;
            StandardDeviation = Math.Sqrt(StandardDeviation);
        }

        /// <summary>
        /// Alias method that calls SelectParent method from IParentSelection interface.
        /// </summary>
        /// <returns></returns>
        public Individual SelectParent()
        {
            return this._parentSelectionInterface.SelectParent(this);
        }

        /// <summary>
        /// Alias method that calls SelectAdults method from IAdultSelection interface.
        /// </summary>
        /// <param name="offspring">Offspring population for the IAdultSelection interface SelectAdults method.</param>
        private void SelectAdults(Population offspring)
        {
            var pop = this;
            this._adultSelectionInterface.SelectAdults(ref pop, offspring);
        }

        /// <summary>
        /// Alias method that calls CreateOffspringPopulation method from IAdultSelection interface.
        /// </summary>
        /// <returns></returns>
        private Population CreateOffspringPopulation()
        {
            return this._adultSelectionInterface.CreateOffspringPopulation(this);
        }

        /// <summary>
        /// Creates offspring population and selects Individuals from offspring population that will become part of the current Parent population.
        /// </summary>
        public void RunNewGenerationCycle()
        {
            Population offspring = CreateOffspringPopulation();
            SelectAdults(offspring);
        }
    }
}
