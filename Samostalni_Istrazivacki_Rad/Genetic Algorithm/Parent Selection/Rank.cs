﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterThesis.Genetic_Algorithm.Individuals;

namespace MasterThesis.Genetic_Algorithm.Parent_Selection
{
    public class Rank : IParentSelection
    {
        public Individual SelectParent(Population population)
        {
            double min = population.MaxFitIndividual.Fitness;
            double max = population.MinFitIndividual.Fitness;
            int n = population.PopulationSize;

            // Sorted descending, 1st is best fit and his rank is n
            List<Individual> sorted = population.Individuals.OrderByDescending(item => item.Fitness).ToList();

            // Change fitness of all Individuals
            int i = 0;
            foreach (Individual individual in sorted)
                individual.Fitness = min + (max - min) * ((n - i++) / (double)(population.PopulationSize - 1));
            population.UpdatePopulationData();
            double randomNumber = Program.RANDOM.NextDouble() * population.TotalFitness;
            int index;
            for (index = 0; index < population.PopulationSize && randomNumber > 0; ++index)
            {
                randomNumber -= population.Individuals.ElementAt(index).Fitness;
            }

            // Revert changes
            population.UpdateFitnessAndPopulationData();
            return population.Individuals.ElementAt(index - 1);
        }
    }
}
