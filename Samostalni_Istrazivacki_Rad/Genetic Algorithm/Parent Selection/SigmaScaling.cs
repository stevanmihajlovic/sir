﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterThesis.Genetic_Algorithm.Individuals;

namespace MasterThesis.Genetic_Algorithm.Parent_Selection
{
    public class SigmaScaling : IParentSelection
    {
        public Individual SelectParent(Population population)
        {
            // Change fitness of all Individuals
            if (population.StandardDeviation != 0)
                foreach (Individual individual in population.Individuals)
                    individual.Fitness = 1 + (individual.Fitness - population.AverageFitness) / (2 * population.StandardDeviation);
            population.UpdatePopulationData();

            double randomNumber = Program.RANDOM.NextDouble() * population.TotalFitness;
            int index;
            for (index = 0; index < population.PopulationSize && randomNumber > 0; ++index)
            {
                randomNumber -= population.Individuals.ElementAt(index).Fitness;
            }

            // Revert changes
            population.UpdateFitnessAndPopulationData();
            return population.Individuals.ElementAt(index - 1);
        }
    }
}
