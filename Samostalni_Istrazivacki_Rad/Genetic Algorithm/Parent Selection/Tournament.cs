﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterThesis.Genetic_Algorithm.Individuals;

namespace MasterThesis.Genetic_Algorithm.Parent_Selection
{
    public class Tournament : IParentSelection
    {
        /// <summary>
        /// Choses random number of Individuals from specified population based on TOURNAMENT_SIZE.
        /// Selects and returns best Individual from these random chosen ones.
        /// </summary>
        /// <param name="population"></param>
        /// <returns></returns>
        public Individual SelectParent(Population population)
        {
            Population newPopulation = new Population(Program.TOURNAMENT_SIZE);
            for (int i = 0; i < Program.TOURNAMENT_SIZE; i++)
                newPopulation.Individuals.Add(population.Individuals[Program.RANDOM.Next(population.PopulationSize)]);
            newPopulation.UpdatePopulationData();
            Individual newIndividual = newPopulation.MaxFitIndividual;
            return newIndividual;
        }
    }
}
