﻿using MasterThesis.Genetic_Algorithm.Individuals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.Genetic_Algorithm.Parent_Selection
{
    public interface IParentSelection
    {
        /// <summary>
        /// Selects and returns one Individual among all Individuals in specified population based on fitness.
        /// </summary>
        /// <param name="population">Specified population.</param>
        /// <returns></returns>
        Individual SelectParent(Population population);
    }
}
