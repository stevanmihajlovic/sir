﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.Genetic_Algorithm.Adult_Selection
{
    public interface IAdultSelection
    {
        /// <summary>
        /// Implementing this interface allows you to decide which Individuals from the Offspring population will become part of the current Parents population
        /// </summary>
        /// <param name="parents">Parents population</param>
        /// <param name="offspring">Offspring population</param>
        void SelectAdults(ref Population parents, Population offspring);

        /// <summary>
        /// Implementing this method allows you to decide how offspring population will be created from specified parent population.
        /// You can decide what to do with ELITISM, with Individuals that were created by CrossOver as well.
        /// For now all the implementation is same, only Overproduction creates twice larger Population.
        /// </summary>
        /// <param name="parents">Specified parent population.</param>
        /// <returns></returns>
        Population CreateOffspringPopulation(Population parents);
    }
}
