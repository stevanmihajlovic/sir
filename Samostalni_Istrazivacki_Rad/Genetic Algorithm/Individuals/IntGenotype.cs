﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.Genetic_Algorithm.Individuals
{
    public abstract class IntGenotype : Individual
    {
        /// <summary>
        /// Gets or sets value representing the gene material.
        /// </summary>
        public List<int> Genes { get; private set; }

        /// <summary>
        /// Gets or sets value representing minimum value of a single gene.
        /// </summary>
        public int MinValue { get; private set; }

        /// <summary>
        /// Gets or sets value representing maximum value of a single gene.
        /// </summary>
        public int MaxValue { get; private set; }

        /// <summary>
        /// Initializes random Individual with specified gene length and boundaries of gene values if provided
        /// </summary>
        /// <param name="geneLength">Specified gene length.</param>
        /// <param name="min">Minimal value boundary of genes</param>
        /// <param name="max">Maximal value boundary of genes</param>
        public IntGenotype(int geneLength, int? min = null, int? max = null)
        {
            MaxValue = max == null ? int.MaxValue : max.Value;
            MinValue = min == null ? 0 : min.Value;
            GeneLength = geneLength;
            Genes = new List<int>(GeneLength);
            for (int i = 0; i < GeneLength; i++)
                Genes.Add(Program.RANDOM.Next(MinValue, MaxValue));
        }

        /// <summary>
        /// Mutates Individual by assigning new values for certain genes depending on MUTATION_RATE.
        /// We use MUTATION_RATE as rate on the level of the genes of the Individual. 
        /// </summary>
        public override void Mutation()
        {
            for (int i = 0; i < GeneLength; i++)
                if (Program.RANDOM.NextDouble() < Program.MUTATION_RATE)
                        Genes[i] = Program.RANDOM.Next(MinValue, MaxValue);
        }

        /// <summary>
        /// Combines genes of this Individual with genes another second provided parent with one cross point.
        /// </summary>
        /// <param name="secondParent">Second provided parent.</param>
        /// <returns>
        /// It generates new Individual(s) or simply creates copies of the Individual 
        /// that called the method and second provided parent depending on CROSSOVER_RATE.
        /// Returns both of the new created Individuals.
        /// </returns>
        public override Individual[] CrossOver(Individual secondParent)
        {
            Individual[] newIndiv = new IntGenotype[2];
            newIndiv[0] = this.MakeACopy();
            newIndiv[1] = secondParent.MakeACopy();

            if (Program.RANDOM.NextDouble() < Program.CROSSOVER_RATE)
            {
                int crossPoint = Program.RANDOM.Next(GeneLength);
                int i;
                for (i = 0; i < crossPoint; i++)
                {
                    ((IntGenotype)newIndiv[0]).Genes.Add(((IntGenotype)secondParent).Genes[i]);
                    ((IntGenotype)newIndiv[1]).Genes.Add(Genes[i]);

                }
                for (; i < GeneLength; i++)
                {
                    ((IntGenotype)newIndiv[0]).Genes.Add(Genes[i]);
                    ((IntGenotype)newIndiv[1]).Genes.Add(((IntGenotype)secondParent).Genes[i]);

                }
            }

            return newIndiv;
        }

        /// <summary>
        /// Represents Individual as array of int values separated by commas.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string asString = "";
            for (int i = 0; i < GeneLength; i++)
                asString = asString + Genes[i] + ", ";
            return asString;
        }
    }
}
