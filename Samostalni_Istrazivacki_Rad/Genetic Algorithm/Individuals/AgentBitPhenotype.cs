﻿using MasterThesis.ANN.Neural_Network;
using MasterThesis.Genetic_Algorithm.Individuals;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MasterThesis.ANN
{
    public class AgentBitPhenotype : BitGenotype
    {
        private int _numberOfInputNeurons = 6;
        private int _numberOfOutputNeurons = 3;

        /// <summary>
        /// Gets or sets a value representing X coordinate of the Agent on the Map.
        /// </summary>
        public int XCoordinate { get; private set; }

        /// <summary>
        /// Gets or sets a value representing Y coordinate of the Agent on the Map.
        /// </summary>
        public int YCoordinate { get; private set; }

        /// <summary>
        /// Gets or sets a value representing Direction of the Agent on the Map.
        /// </summary>
        public Direction Direction { get; private set; }

        /// <summary>
        /// Gets or sets a value representing Map on which Agent is moving on.
        /// </summary>
        public Grid Map { get; set; }

        private int _lengthOfSingleWeightAsBitArray;
        private int _maxSteps;
        private double _fitnessAward;
        private double _fitnessPenalty;

        /// <summary>
        /// Used for instantiating copies.
        /// </summary>
        /// <param name="geneLength"></param>
        public AgentBitPhenotype(int geneLength)
            : base(geneLength)
        {

        }

        /// <summary>
        /// Instantiates new instance of Agent and sets his max steps, award and penalty based on static properties and sets the specified ANN and weight bit length. 
        /// </summary>
        /// <param name="ann">Specified ANN.</param>
        /// <param name="lengthOfSingleWeightAsBitArray">Specified weight bit length.</param>
        public AgentBitPhenotype(ArtificialNeuralNetwork ann, int lengthOfSingleWeightAsBitArray)
            : base(ann, lengthOfSingleWeightAsBitArray)
        {
            Map = Program.MAP.MakeACopy();  // check this one out
            _lengthOfSingleWeightAsBitArray = lengthOfSingleWeightAsBitArray;
            _maxSteps = Program.MAX_STEPS;
            _fitnessAward = Program.AWARD_FACTOR;
            _fitnessPenalty = Program.PENALTY_FACTOR;
        }

        /// <summary>
        /// Positions Agent at the center of the Map.
        /// </summary>
        public void RestartPosition()
        {
            YCoordinate = Map.Dimension / 2;
            XCoordinate = Map.Dimension / 2;
            Map.Tiles[XCoordinate][YCoordinate] = TileContent.Nothing;
            Direction = Direction.Up;   // Check this out
        }

        /// <summary>
        /// Changes direction of Agent to the left based on current direction.
        /// </summary>
        public void TurnLeft()
        {
            Direction = (Direction)(((int)Direction - 1 + 4) % 4);
        }

        /// <summary>
        /// Changes direction of Agent to the right based on current direction.
        /// </summary>
        public void TurnRight()
        {
            Direction = (Direction)(((int)Direction + 1) % 4);
        }

        /// <summary>
        /// Gets the Tile on the left side of the Agent based on current direction.
        /// </summary>
        /// <returns></returns>
        public TileContent GetLeftTile()
        {
            TileContent temp = TileContent.Nothing;
            switch (Direction)
            {
                case Direction.Up:
                    {
                        temp = Map.GetLeftTile(XCoordinate, YCoordinate);
                        break;
                    }
                case Direction.Right:
                    {
                        temp = Map.GetTopTile(XCoordinate, YCoordinate);
                        break;
                    }
                case Direction.Down:
                    {
                        temp = Map.GetRightTile(XCoordinate, YCoordinate);
                        break;
                    }
                case Direction.Left:
                    {
                        temp = Map.GetBottomTile(XCoordinate, YCoordinate);
                        break;
                    }
            }
            return temp;
        }

        /// <summary>
        /// Gets the Tile on the right side of the Agent based on current direction.
        /// </summary>
        /// <returns></returns>
        public TileContent GetRightTile()
        {
            TileContent temp = TileContent.Nothing;
            switch (Direction)
            {
                case Direction.Up:
                    {
                        temp = Map.GetRightTile(XCoordinate, YCoordinate);
                        break;
                    }
                case Direction.Right:
                    {
                        temp = Map.GetBottomTile(XCoordinate, YCoordinate);
                        break;
                    }
                case Direction.Down:
                    {
                        temp = Map.GetLeftTile(XCoordinate, YCoordinate);
                        break;
                    }
                case Direction.Left:
                    {
                        temp = Map.GetTopTile(XCoordinate, YCoordinate);
                        break;
                    }
            }
            return temp;
        }

        /// <summary>
        /// Gets the Tile in front of the Agent based on current direction.
        /// </summary>
        /// <returns></returns>
        public TileContent GetForwardTile()
        {
            TileContent temp = TileContent.Nothing;
            switch (Direction)
            {
                case Direction.Up:
                    {
                        temp = Map.GetTopTile(XCoordinate, YCoordinate);
                        break;
                    }
                case Direction.Right:
                    {
                        temp = Map.GetRightTile(XCoordinate, YCoordinate);
                        break;
                    }
                case Direction.Down:
                    {
                        temp = Map.GetBottomTile(XCoordinate, YCoordinate);
                        break;
                    }
                case Direction.Left:
                    {
                        temp = Map.GetLeftTile(XCoordinate,YCoordinate);
                        break;
                    }
            }
            return temp;
        }

        /// <summary>
        /// Moves Agent to the tile in front of him based on current direction.
        /// </summary>
        public void Move()
        {
            switch (Direction)
            {
                case Direction.Up:
                    {
                        YCoordinate = (YCoordinate + Map.Dimension - 1) % Map.Dimension;
                        break;
                    }
                case Direction.Right:
                    {
                        XCoordinate = (XCoordinate + 1) % Map.Dimension;
                        break;
                    }
                case Direction.Down:
                    {
                        YCoordinate = (YCoordinate + 1) % Map.Dimension;
                        break;
                    }
                case Direction.Left:
                    {
                        XCoordinate = (XCoordinate + Map.Dimension - 1) % Map.Dimension;
                        break;
                    }
            }
        }

        public override void CalculateFitness()
        {
            //if (!dynamic)
                Grid temp = Map.MakeACopy();
            //else
                //Map = new Grid(Map.FoodProbability, Map.PoisonProbability, Map.Dimension);
            RestartPosition();
            ANN.UpdateAllWeights(DecodeGenotype());
            Fitness = 0;
            for (int i = 0; i < _maxSteps; i++)
            {
                ANN.ResetAllNeuronValues();
                List<double> inputNeuronValues = FillInputNeuronValues(GetLeftTile(), GetForwardTile(), GetRightTile());
                ANN.UpdateInputNeuronsValues(inputNeuronValues);
                int maxOutputnNeuronIndex = ANN.CaluclateOutputNeuronValues();
                SetDirection(maxOutputnNeuronIndex);
                switch (GetForwardTile())
                {
                    case TileContent.Food:
                        {
                            Fitness += _fitnessAward;
                            break;
                        }
                    case TileContent.Poison:
                        {
                            Fitness -= _fitnessPenalty;
                            break;
                        }
                }
                Move();
                Map.Tiles[XCoordinate][YCoordinate] = TileContent.Nothing;
            }
            Map = temp;
        }

        /// <summary>
        /// Decodes the weight from bit into double representation.
        /// </summary>
        /// <returns></returns>
        public List<double> DecodeGenotype()
        {
            int k;
            List<double> arrayOfWeights = new List<double>();
            for (int i = 0; i < ANN.TotalNumberOfWeights; i++)
            {
                double value = 0;
                k = 1;
                for (int j = 0; j < _lengthOfSingleWeightAsBitArray; j++)
                {
                    if (Genes.Get((int)(i * _lengthOfSingleWeightAsBitArray + j)))
                    {
                        value += k;
                    }
                    k = k << 1;
                }
                arrayOfWeights.Add(value / ((double)k - 1));
            }
            return arrayOfWeights;
        }

        /// <summary>
        /// Fills the value of Input Neurons based on tiles Agent is surrounded with.
        /// </summary>
        /// <param name="left">Tile on the left side of the Agent.</param>
        /// <param name="forward">Tile in front of the Agent.</param>
        /// <param name="right">Tile on the right side of the Agent.</param>
        /// <returns></returns>
        public List<double> FillInputNeuronValues(TileContent left, TileContent forward, TileContent right)
        {
            double [] inputs = new double[_numberOfInputNeurons];
            if (forward == TileContent.Food)
                inputs[0] = 1;
            if (forward == TileContent.Poison)
                inputs[3] = 1;
            if (left == TileContent.Food)
                inputs[1] = 1;
            if (left == TileContent.Poison)
                inputs[4] = 1;
            if (right == TileContent.Food)
                inputs[2] = 1;
            if (right == TileContent.Poison)
                inputs[5] = 1;
            return inputs.ToList();            
        }

        /// <summary>
        /// Sets Direction of the agent based on the index of Neuron in Output Layer with the highest value.
        /// </summary>
        /// <param name="maxOutputNeuronIndex">Index of Neuron in Output Layer with the highest value.</param>
        public void SetDirection(int maxOutputNeuronIndex)
        {
            if (maxOutputNeuronIndex == 1)
            {
                TurnLeft();
            }
            if (maxOutputNeuronIndex == 2)
            {
                TurnRight();
            }
        }

        /// <summary>
        /// Creates an identical copy of the Agent. Used for Crossover and selection mechanisms.
        /// </summary>
        /// <returns></returns>
        public override Individual MakeACopy()  //check this out
        {
            AgentBitPhenotype agent = new AgentBitPhenotype(ANN.TotalNumberOfWeights * _lengthOfSingleWeightAsBitArray);
            agent.Genes = new BitArray(this.Genes.Clone() as BitArray);
            agent.Map = this.Map.MakeACopy();
            agent.ANN = this.ANN;
            agent.XCoordinate = this.XCoordinate;
            agent.YCoordinate = this.YCoordinate;
            agent._fitnessAward = this._fitnessAward;
            agent._fitnessPenalty = this._fitnessPenalty;
            agent._lengthOfSingleWeightAsBitArray = this._lengthOfSingleWeightAsBitArray;
            agent._maxSteps = this._maxSteps;
            return agent;
        }

        public override int NumberOfInputNeurons()
        {
            return _numberOfInputNeurons;
        }

        public override int NumberOfOutputNeurons()
        {
            return _numberOfOutputNeurons;
        }

        /// <summary>
        /// Draws the Agent on the specified panel using specified graphics, brush and pen.
        /// </summary>
        /// <param name="gridPanel">Specified panel.</param>
        /// <param name="pen">Specified pen.</param>
        /// <param name="brush">Specified brush.</param>
        /// <param name="formGraphics">Specified graphics.</param>
        public void DrawOnPanel(Panel gridPanel, Pen pen, SolidBrush brush, Graphics formGraphics)
        {
            int stepX = gridPanel.Width / Map.Dimension;
            int stepY = gridPanel.Height / Map.Dimension;
            int margine = 2 * Map.Dimension;

            // Map
            for (int i = 0; i < Map.Dimension; i++)
            {
                for (int j = 0; j < Map.Dimension; j++)
                {
                    switch (Map.Tiles[i][j])
                    {
                        case TileContent.Nothing:
                            {
                                break;
                            }
                        case TileContent.Food:
                            {
                                brush.Color = Color.Green;
                                formGraphics.FillEllipse(brush, new Rectangle(gridPanel.Location.X + i * stepX + margine / 2, gridPanel.Location.Y + j * stepY + margine / 2, stepX - margine, stepY - margine));
                                break;
                            }
                        case TileContent.Poison:
                            {
                                brush.Color = Color.Red;
                                formGraphics.FillEllipse(brush, new Rectangle(gridPanel.Location.X + i * stepX + margine / 2, gridPanel.Location.Y + j * stepY + margine / 2, stepX - margine, stepY - margine));
                                break;
                            }
                    }
                }
            }

            pen.Width = 3;

            // Agent            
            if (Direction != Direction.Down)
            {
                formGraphics.DrawLine(pen, gridPanel.Location.X + XCoordinate * stepX + stepX / 2, gridPanel.Location.Y + YCoordinate * stepY + stepY / 2,
                gridPanel.Location.X + XCoordinate * stepX + stepX / 2, gridPanel.Location.Y + YCoordinate * stepY);
            }

            if (Direction != Direction.Up)
            {
                formGraphics.DrawLine(pen, gridPanel.Location.X + XCoordinate * stepX + stepX / 2, gridPanel.Location.Y + YCoordinate * stepY + stepY / 2,
                gridPanel.Location.X + XCoordinate * stepX + stepX / 2, gridPanel.Location.Y + YCoordinate * stepY + stepY);
            }
            if (Direction != Direction.Left)
            {
                formGraphics.DrawLine(pen, gridPanel.Location.X + XCoordinate * stepX + stepX / 2, gridPanel.Location.Y + YCoordinate * stepY + stepY / 2,
                gridPanel.Location.X + XCoordinate * stepX + stepX, gridPanel.Location.Y + YCoordinate * stepY + stepY / 2);
            }
            if (Direction != Direction.Right)
            {
                formGraphics.DrawLine(pen, gridPanel.Location.X + XCoordinate * stepX + stepX / 2, gridPanel.Location.Y + YCoordinate * stepY + stepY / 2,
                gridPanel.Location.X + XCoordinate * stepX, gridPanel.Location.Y + YCoordinate * stepY + stepY / 2);
            }
            brush.Color = Color.Blue;
            formGraphics.FillEllipse(brush, new Rectangle(gridPanel.Location.X + XCoordinate * stepX + stepX / 4, gridPanel.Location.Y + YCoordinate * stepY + stepY / 4, stepX / 2, stepY / 2));
        }
    }
}
