﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterThesis.Genetic_Algorithm.Individuals
{
    public abstract class IntUniqueGenotype : Individual
    {
        /// <summary>
        /// Gets or sets value representing the gene material.
        /// </summary>
        public List<int> Genes { get; private set; }

        /// <summary>
        /// Initializes random Individual with specified gene length.
        /// All Genes will be unique values from 1 to specified gene length.
        /// </summary>
        /// <param name="geneLength">Specified gene length.</param>
        public IntUniqueGenotype(int geneLength)
        {
            Genes = new List<int>();
            GeneLength = geneLength;
            List<int> lookup = new List<int>();
            int i;

            for (i = 0; i < GeneLength; i++)
                lookup.Add(i + 1);

            for (i = 0; i < GeneLength; i++)
            {
                int temp = Program.RANDOM.Next(lookup.Count);
                Genes.Add(lookup[temp]);
                lookup.RemoveAt(temp);
            }
        }

        /// <summary>
        /// Mutates Individual by switching places of two genes.
        /// We use MUTATION_RATE as rate on the level of the whole Individual.
        /// </summary>
        public override void Mutation()
        {
            if (Program.RANDOM.NextDouble() < Program.MUTATION_RATE)
            {
                int firstIndex = Program.RANDOM.Next(GeneLength);
                int secondIndex = Program.RANDOM.Next(GeneLength);

                int temp = Genes[firstIndex];
                Genes[firstIndex] = Genes[secondIndex];
                Genes[secondIndex] = temp;
            }
        }

        /// <summary>
        /// Combines genes of this Individual with genes another second provided parent with one cross point.
        /// Retains uniqueness in each of the new created Individuals.
        /// </summary>
        /// <param name="secondParent">Second provided parent.</param>
        /// <returns>
        /// It generates new Individual(s) or simply creates copies of the Individual 
        /// that called the method and second provided parent depending on CROSSOVER_RATE.
        /// Returns both of the new created Individuals.
        /// </returns>
        public override Individual[] CrossOver(Individual secondParent)
        {
            Individual[] newIndiv = new IntGenotype[2];
            newIndiv[0] = this.MakeACopy();
            newIndiv[1] = secondParent.MakeACopy();

            if (Program.RANDOM.NextDouble() < Program.CROSSOVER_RATE)
            {
                IntUniqueGenotype secondParentInstance = (IntUniqueGenotype)secondParent;
                List<int> firstParentGenes = new List<int>();
                List<int> secondParentGenes = new List<int>();
                int crossPoint = Program.RANDOM.Next(GeneLength);
                int i;

                for (i = 0; i < crossPoint; i++)
                {
                    firstParentGenes.Add(this.Genes[i]);
                    secondParentGenes.Add(secondParentInstance.Genes[i]);
                }

                for (i = 0; i < GeneLength; i++)
                {
                    if (!firstParentGenes.Contains(secondParentInstance.Genes[i]))
                        firstParentGenes.Add(secondParentInstance.Genes[i]);
                    if (!secondParentGenes.Contains(this.Genes[i]))
                        secondParentGenes.Add(this.Genes[i]);
                }

                ((IntUniqueGenotype) newIndiv[0]).Genes = firstParentGenes;
                ((IntUniqueGenotype) newIndiv[1]).Genes = secondParentGenes;
            }

            return newIndiv;
        }

        /// <summary>
        /// Represents Individual as array of int values separated by commas.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string asString = "";
            for (int i = 0; i < GeneLength; i++)
                asString = asString + Genes[i] + ", ";
            return asString;
        }
    }
}
