﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MasterThesis
{
    public partial class AddHiddenLayer : Form
    {
        private ANN_Evolved_Agent aNN;

        /// <summary>
        /// Initializes Pop-up window with reference to the specified ANN whose properties will be filled using this Pop-up.
        /// </summary>
        /// <param name="aNN_Evolved_Agent">reference to the specified ANN.</param>
        public AddHiddenLayer(ANN_Evolved_Agent aNN_Evolved_Agent)
        {
            InitializeComponent();
            aNN = aNN_Evolved_Agent;
        }

        /// <summary>
        /// Cancels the filling of properties.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Fills the properties of ANN with input from the Pop-up.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (Int32.TryParse(textBoxNeuronsPerLayer.Text, out int intTemp))
            {
                aNN.NeuronsPerHiddenLayer.Add(intTemp);
                aNN.BiasesPerHiddenLayer.Add(checkBoxBiasNeuron.Checked ? 1 : 0);
                aNN.listBoxHiddenLayers.Items.Add("Hidden layer " + aNN.NeuronsPerHiddenLayer.Count + " has " + intTemp + " neurons and " + aNN.BiasesPerHiddenLayer.Last() + " biases.");
                this.Close();
            }
            else
            {
                MessageBox.Show("Wrong format for Neurons per layer parameter");
            }
        }
    }
}
