﻿using MasterThesis.ANN;
using MasterThesis.Genetic_Algorithm;
using MasterThesis.Genetic_Algorithm.Individuals;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MasterThesis
{
    public partial class ANN_Evolved_Agent : Form
    {
        /// <summary>
        /// Gets or sets a value representing number of Neurons per hidden layer in the ANN.
        /// </summary>
        public List<int> NeuronsPerHiddenLayer { get; set; }

        /// <summary>
        /// Gets or sets a value representing number of Bias Neurons per hidden layer in the ANN.
        /// </summary>
        public List<int> BiasesPerHiddenLayer { get; set; }

        private int _generationsNumber;
        private int _populationSize;
        private int _weightBitLength;
        private double _probabilityFood;
        private double _probabilityPoison;
        private int _mapDimensions;
        
        private Population _population;
        private Individual _agent;

        private int _defGenerationsNumber = 100;
        private int _defPopulationSize = 100;
        private int _defWeightBitLength = 10;
        private double _defCrossOverRate = 0.9;    //per individual
        private double _defMutationRate = 0.01;     //not per individual but per bit
        private bool _defElitism = true;
        private int _defOverproductionScale = 2;
        private int _defTournamentSize = 5;
        private double _defFoodProbability = 0.33;
        private double _defPoisonProbability = 0.33;
        private int _defMapDimensions = 10;
        private int _defMaxSteps = 60;
        private double _defAwardFactor = 5;
        private double _defPenaltyFactor = 30;

        private string _stringAverageFitness = "Average Fitness";
        private string _stringdeviation = "Standard Deviation";
        private string _stringMaxFitness = "Max Fitness";
        private bool _drawing;

        /// <summary>
        /// Default constructor that initializes things on start.
        /// </summary>
        public ANN_Evolved_Agent()
        {
            InitializeComponent();

            comboBoxParent.DataSource = Enum.GetValues(typeof(ParentSelection));
            comboBoxAdult.DataSource = Enum.GetValues(typeof(AdultSelection));
            comboBoxProblemType.DataSource = Enum.GetValues(typeof(ProblemType));
            comboBoxActivationFunction.DataSource = Enum.GetValues(typeof(ActivationFunction));

            NeuronsPerHiddenLayer = new List<int>();
            BiasesPerHiddenLayer = new List<int>();

            _generationsNumber = _defGenerationsNumber;
            _populationSize = _defPopulationSize;
            _weightBitLength = _defWeightBitLength;
            Program.CROSSOVER_RATE = _defCrossOverRate;
            Program.MUTATION_RATE = _defMutationRate;
            Program.TOURNAMENT_SIZE = _defTournamentSize;
            Program.OVERPRODUCTION_SCALE = _defOverproductionScale;
            _probabilityFood = _defFoodProbability;
            _probabilityPoison = _defPoisonProbability;
            _mapDimensions = _defMapDimensions;
            Program.MAX_STEPS = _defMaxSteps;
            Program.AWARD_FACTOR = _defAwardFactor;
            Program.PENALTY_FACTOR = _defPenaltyFactor;
            Program.ELITISM = _defElitism;

            comboBoxParent.SelectedIndex = 0;
            comboBoxAdult.SelectedIndex = 0;
            comboBoxProblemType.SelectedIndex = 0;
            comboBoxActivationFunction.SelectedIndex = 0;

            Program.RANDOM = new Random();

            textBoxPopulationSize.Text = _populationSize.ToString();
            textBoxWeightBitLength.Text = _weightBitLength.ToString();
            textBoxCrossOverRate.Text = Program.CROSSOVER_RATE.ToString();
            textBoxMutationRate.Text = Program.MUTATION_RATE.ToString();
            textBoxTournamentSize.Text = Program.TOURNAMENT_SIZE.ToString();
            textBoxOverProductionScale.Text = Program.OVERPRODUCTION_SCALE.ToString();
            textBoxFoodProbability.Text = _probabilityFood.ToString();
            textBoxPoisonProbability.Text = _probabilityPoison.ToString();
            textBoxMapDimensions.Text = _mapDimensions.ToString();
            textBoxMaxSteps.Text = Program.MAX_STEPS.ToString();
            textBoxAwardFactor.Text = Program.AWARD_FACTOR.ToString();
            textBoxPenaltyFactor.Text = Program.PENALTY_FACTOR.ToString();

            checkBoxElitism.Checked = Program.ELITISM;


            //more to come

            Program.ELITISM = _defElitism;
        }

        /// <summary>
        /// Tries to parse all the input from the Form.
        /// </summary>
        private void Initialize()
        {
            //chart
            chart1.Series[_stringAverageFitness].Points.Clear();
            chart1.Series[_stringMaxFitness].Points.Clear();
            chart1.Series[_stringdeviation].Points.Clear();
            chart1.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            chart1.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            
            //dinamicFlatland = chkDinamyc.Checked;
            //TrainOn5Scenarios = chb5scenarios.Checked;

            double doubleTemp;
            int intTemp;

            if (Int32.TryParse(textBoxGenerationsNumber.Text, out intTemp))
                _generationsNumber = intTemp;
            else
                _generationsNumber = _defGenerationsNumber;

            if (Int32.TryParse(textBoxPopulationSize.Text, out intTemp))
                _populationSize = intTemp;
            else
                _populationSize = _defPopulationSize;

            if (Int32.TryParse(textBoxWeightBitLength.Text, out intTemp))
                _weightBitLength = intTemp;
            else
                _weightBitLength = _defWeightBitLength;

            if (Double.TryParse(textBoxCrossOverRate.Text, out doubleTemp))
                Program.CROSSOVER_RATE = doubleTemp;
            else
                Program.CROSSOVER_RATE = _defCrossOverRate;

            if (Double.TryParse(textBoxMutationRate.Text, out doubleTemp))
                Program.MUTATION_RATE = doubleTemp;
            else
                Program.MUTATION_RATE = _defMutationRate;

            if (Int32.TryParse(textBoxOverProductionScale.Text, out intTemp))
                Program.OVERPRODUCTION_SCALE = intTemp;
            else
                Program.OVERPRODUCTION_SCALE = _defOverproductionScale;

            if (Int32.TryParse(textBoxTournamentSize.Text, out intTemp))
                Program.TOURNAMENT_SIZE = intTemp;
            else
                Program.TOURNAMENT_SIZE = _defTournamentSize;

            if (Double.TryParse(textBoxFoodProbability.Text, out doubleTemp))
                _probabilityFood = doubleTemp;
            else
                _probabilityFood = _defFoodProbability;

            if (Double.TryParse(textBoxPoisonProbability.Text, out doubleTemp))
                _probabilityPoison = doubleTemp;
            else
                _probabilityPoison = _defPoisonProbability;

            if (Int32.TryParse(textBoxMapDimensions.Text, out intTemp))
                _mapDimensions = intTemp;
            else
                _mapDimensions = _defMapDimensions;

            if (Int32.TryParse(textBoxMaxSteps.Text, out intTemp))
                Program.MAX_STEPS = intTemp;
            else
                Program.MAX_STEPS = _defMaxSteps;

            if (Double.TryParse(textBoxAwardFactor.Text, out doubleTemp))
                Program.AWARD_FACTOR = doubleTemp;
            else
                Program.AWARD_FACTOR = _defAwardFactor;

            if (Double.TryParse(textBoxPenaltyFactor.Text, out doubleTemp))
                Program.PENALTY_FACTOR = doubleTemp;
            else
                Program.PENALTY_FACTOR = _defPoisonProbability;

            Program.ELITISM = checkBoxElitism.Checked;

            switch (comboBoxProblemType.SelectedItem)
            {
                case ProblemType.ANNBitAgent:
                    {
                        _agent = new AgentBitPhenotype(10);
                        break;
                    }
                case ProblemType.ANNDoubleAgent:
                    {
                        _agent = new AgentDoublePhenotype(10);
                        break;
                    }
                default:
                    throw new NotImplementedException("Problem type not implemented.");
            }
            Program.ANN = new FeedForwardANN(_agent.NumberOfInputNeurons(), _agent.NumberOfOutputNeurons(), NeuronsPerHiddenLayer, BiasesPerHiddenLayer);

            Program.MAP = new Grid(_probabilityFood, _probabilityPoison, _mapDimensions);

            _population = new Population(_populationSize, _weightBitLength);
            _population.UpdateFitnessAndPopulationData();
            //currentFlatland = 0;

            //flatlands = new Flatland[TrainOn5Scenarios ? 5 : 1];
            //cmbScenario.Items.Clear();
            //for (int i = 0; i < flatlands.Length; i++)
            //{
            //    flatlands[i] = new Flatland(probabilityFood, probabilityPoison);
            //    cmbScenario.Items.Add(i);
            //}
            //cmbScenario.Items.Add("Random");
            //agent = new Agent();
            //agent.flatland = flatlands[currentFlatland].makeACopy();

        }

        /// <summary>
        /// Sets visibility of TournamentSize TextBox and Label to the specified value and updates static field representing Parent Selection.
        /// If Tournament Size is selected in the Parent Selection Combo Box then true, otherwise false.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxParent_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetTournamentSizeVisibility((ParentSelection)comboBoxParent.SelectedItem == ParentSelection.Tournament);
            Program.CHOSEN_PARENT_SELECTION = (ParentSelection)comboBoxParent.SelectedItem;
        }

        /// <summary>
        /// Sets visibility of TournamentSize TextBox and Label to the specified value.
        /// </summary>
        /// <param name="isVisible">Specified value.</param>
        private void SetTournamentSizeVisibility(bool isVisible)
        {
            labelTournamentSize.Visible = textBoxTournamentSize.Visible = isVisible;
        }

        /// <summary>
        /// Sets visibility of OverProduction TextBox and Label to the specified value and updates static field representing Adult Selection.
        /// If Over Production is selected in the Adult Selection Combo Box then true, otherwise false.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxAdult_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetOverProductionScaleVisibility((AdultSelection)comboBoxAdult.SelectedItem == AdultSelection.OverProduction);
            Program.CHOSEN_ADULT_SELECTION = (AdultSelection)comboBoxAdult.SelectedItem;
        }

        /// <summary>
        /// Sets visibility of OverProduction TextBox and Label to the specified value.
        /// </summary>
        /// <param name="isVisible">Specified value.</param>
        private void SetOverProductionScaleVisibility(bool isVisible)
        {
            labelOverProductionScale.Visible = textBoxOverProductionScale.Visible = isVisible;
        }

        /// <summary>
        /// Sets visibility of Problem Specific UI Controls on form to the specified value and updates static field representing Problem Type.
        /// If ANNBitAgent or ANNDoubleAgents are selected in Problem Type Combo box then true, otherwise false.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxProblemType_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetProblemSpecificParametersVisibility((ProblemType)comboBoxProblemType.SelectedItem == ProblemType.ANNBitAgent ||
                                                   (ProblemType)comboBoxProblemType.SelectedItem == ProblemType.ANNDoubleAgent);
            Program.CHOSEN_PROBLEM_TYPE = (ProblemType)comboBoxProblemType.SelectedItem;
        }
        
        /// <summary>
        /// Sets visibility of Problem Specific UI Controls on form to the specified value.
        /// </summary>
        /// <param name="isVisible">Specified value</param>
        private void SetProblemSpecificParametersVisibility(bool isVisible)
        {
            labelFoodProbability.Visible = textBoxFoodProbability.Visible = isVisible;

            labelPoisonProbability.Visible = textBoxPoisonProbability.Visible = isVisible;

            labelMapDimensions.Visible = textBoxMapDimensions.Visible = isVisible;

            labelMaxSteps.Visible = textBoxMaxSteps.Visible = isVisible;

            labelAwardFactor.Visible = textBoxAwardFactor.Visible = isVisible;

            labelPenaltyFactor.Visible = textBoxPenaltyFactor.Visible = isVisible;
        }

        /// <summary>
        /// Updates static field representing Activation Function.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBoxActivationFunction_SelectedIndexChanged(object sender, EventArgs e)
        {
            Program.CHOSEN_ACTIVATION_FUNCTION = (ActivationFunction)comboBoxActivationFunction.SelectedItem;
        }

        /// <summary>
        /// Clears settings for hidden layers in ANN.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClearHiddenLayers_Click(object sender, EventArgs e)
        {
            listBoxHiddenLayers.Items.Clear();
            NeuronsPerHiddenLayer.Clear();
            BiasesPerHiddenLayer.Clear();
        }

        /// <summary>
        /// Shows Pop-up for adding hidden layers to the ANN.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAddHiddenLayer_Click(object sender, EventArgs e)
        {
            AddHiddenLayer addHiddenLayer = new AddHiddenLayer(this);
            addHiddenLayer.ShowDialog();
        }

        /// <summary>
        /// Starts the ANN training process
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonTrain_Click(object sender, EventArgs e)
        {
            Initialize();
            labelStatus.Text = "Status: Training";
            Stopwatch timer = new Stopwatch();
            timer.Start();
            this.Refresh();

            for (int i = 0; i < _generationsNumber; i++)
            {
                _population.RunNewGenerationCycle();

                //chart
                chart1.Series[_stringMaxFitness].Points.AddXY(i, _population.MaxFitIndividual.Fitness);
                chart1.Series[_stringdeviation].Points.AddXY(i, _population.StandardDeviation);
                chart1.Series[_stringAverageFitness].Points.AddXY(i, _population.AverageFitness);
            }
            timer.Stop();
            labelStatus.Text = "Status: Completed in " + Math.Round(timer.Elapsed.TotalSeconds, 2) +"s";
            buttonTest.Enabled = true;
            _agent = _population.MaxFitIndividual;
            this.Refresh();
        }

        /// <summary>
        /// Draws problem specific UI
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e)
        {
            System.Drawing.Pen pen = new System.Drawing.Pen(Color.Black, 1);
            System.Drawing.SolidBrush brush = new System.Drawing.SolidBrush(System.Drawing.Color.Red);
            System.Drawing.Graphics formGraphics;
            formGraphics = this.CreateGraphics();

            int gridSize = _mapDimensions;
            int stepX = gridPanel.Width / gridSize;
            int stepY = gridPanel.Height / gridSize;


            for (int i = 0; i < gridSize + 1; i++)
            {
                // Vertical line
                formGraphics.DrawLine(pen, gridPanel.Location.X + i * stepX, gridPanel.Location.Y,
                gridPanel.Location.X + i * stepX, gridPanel.Location.Y + gridPanel.Height / gridSize * gridSize);

                // Horizontal line
                formGraphics.DrawLine(pen, gridPanel.Location.X, gridPanel.Location.Y + i * stepY,
                gridPanel.Location.X + gridPanel.Width / gridSize * gridSize, gridPanel.Location.Y + i * stepY);
            }
            if (!_drawing)
            {
                return;
            }

            // Play mode
            if (_agent == null)
            {
                pen.Dispose();
                formGraphics.Dispose();
                return;
            }

            switch (comboBoxProblemType.SelectedItem)
            {
                case ProblemType.ANNBitAgent:
                    {
                        ((AgentBitPhenotype)_agent).DrawOnPanel(gridPanel, pen, brush, formGraphics);
                        break;
                    }
                case ProblemType.ANNDoubleAgent:
                    {
                        ((AgentDoublePhenotype)_agent).DrawOnPanel(gridPanel, pen, brush, formGraphics);
                        break;
                    }
                default:
                    throw new NotImplementedException("Problem type not implemented.");
            }
            pen.Dispose();
            formGraphics.Dispose();
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {
            _drawing = true;

            switch (comboBoxProblemType.SelectedItem)
            {
                case ProblemType.ANNBitAgent:
                    {
                        AgentBitPhenotypePlayGame((AgentBitPhenotype)_agent);
                        break;
                    }
                case ProblemType.ANNDoubleAgent:
                    {
                        AgentDoublePhenotypePlayGame((AgentDoublePhenotype)_agent);
                        break;
                    }
                default:
                    throw new NotImplementedException("Problem type not implemented.");
            }

            _drawing = false;
        }

        /// <summary>
        /// Visual simulation of best Individual in the Population and his performance.
        /// </summary>
        /// <param name="agentBitPhenotype">best Individual in the Population.</param>
        public void AgentBitPhenotypePlayGame(AgentBitPhenotype agentBitPhenotype)
        {
            AgentBitPhenotype agent = agentBitPhenotype;

            Grid temp = Program.MAP.MakeACopy();
            agent.RestartPosition();
            Program.ANN.UpdateAllWeights(agent.DecodeGenotype());
            this.Refresh();
            int food = 0;
            int poison = 0;
            for (int i = 0; i < Program.MAX_STEPS; i++)
            {
                labelSteps.Text = "Steps: " + i + ", Food: " + food + ", Poison: " + poison;
                Program.ANN.ResetAllNeuronValues();
                List<double> inputNeuronValues = agent.FillInputNeuronValues(agent.GetLeftTile(), agent.GetForwardTile(), agent.GetRightTile());
                Program.ANN.UpdateInputNeuronsValues(inputNeuronValues);
                int maxOutputnNeuronIndex = Program.ANN.CaluclateOutputNeuronValues();
                agent.SetDirection(maxOutputnNeuronIndex);
                agent.Move();
                if (agent.Map.Tiles[agent.XCoordinate][agent.YCoordinate] == TileContent.Food)
                {
                    food++;
                }
                else if (agent.Map.Tiles[agent.XCoordinate][agent.YCoordinate] == TileContent.Poison)
                {
                    poison++;
                }
                agent.Map.Tiles[agent.XCoordinate][agent.YCoordinate] = TileContent.Nothing;
                this.Refresh();
                System.Threading.Thread.Sleep(250);
            }
            agent.Map = temp;
            labelSteps.Text = "Done!" + " Food: " + food + ", Poison: " + poison;

        }

        /// <summary>
        /// Visual simulation of best Individual in the Population and his performance.
        /// </summary>
        /// <param name="agentBitPhenotype">best Individual in the Population.</param>
        public void AgentDoublePhenotypePlayGame(AgentDoublePhenotype agentBitPhenotype)
        {
            AgentDoublePhenotype agent = agentBitPhenotype;

            Grid temp = Program.MAP.MakeACopy();
            agent.RestartPosition();
            Program.ANN.UpdateAllWeights(agent.Genes);
            this.Refresh();
            int food = 0;
            int poison = 0;
            for (int i = 0; i < Program.MAX_STEPS; i++)
            {
                labelSteps.Text = "Steps: " + i + ", Food: " + food + ", Poison: " + poison;
                Program.ANN.ResetAllNeuronValues();
                List<double> inputNeuronValues = agent.FillInputNeuronValues(agent.GetLeftTile(), agent.GetForwardTile(), agent.GetRightTile());
                Program.ANN.UpdateInputNeuronsValues(inputNeuronValues);
                int maxOutputnNeuronIndex = Program.ANN.CaluclateOutputNeuronValues();
                agent.SetDirection(maxOutputnNeuronIndex);
                agent.Move();
                if (agent.Map.Tiles[agent.XCoordinate][agent.YCoordinate] == TileContent.Food)
                {
                    food++;
                }
                else if (agent.Map.Tiles[agent.XCoordinate][agent.YCoordinate] == TileContent.Poison)
                {
                    poison++;
                }
                agent.Map.Tiles[agent.XCoordinate][agent.YCoordinate] = TileContent.Nothing;
                this.Refresh();
                System.Threading.Thread.Sleep(250);
            }
            agent.Map = temp;
            labelSteps.Text = "Done!" + " Food: " + food + ", Poison: " + poison;

        }
    }
}
